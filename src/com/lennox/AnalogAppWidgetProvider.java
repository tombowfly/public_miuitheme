/*
 * Copyright (C) 2014 Lennox Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.theme;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.RemoteViews;

import com.lennox.utils.DefaultPackages;

import com.lennox.theme.miui.R;

/**
 * Simple widget to show analog clock.
 */
public class AnalogAppWidgetProvider extends AppWidgetProvider {

    static final String TAG = "AnalogAppWidgetProvider";

    static int sClocks;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        if (l.b.DEBUG) Log.d(TAG, "onUpdate");

        final int N = appWidgetIds.length;

        StringBuilder clockDials = new StringBuilder("");
        String packageName = context.getPackageName();
        final Resources resources = context.getResources();
        for ( int i = 0; i < Integer.MAX_VALUE; i++ ) {
            int clockRes = resources.getIdentifier("clock_" + i, "layout", packageName);
            if (clockRes == 0) {
                sClocks = i - 1;
                break;
            }
        }

        for (int i=0; i<N; i++) {
            int selectedPos = AnalogAppWidgetConfigure.loadClockPref(context, appWidgetIds[i]);
            if (selectedPos < 0 || selectedPos > sClocks) selectedPos = 0;
            String resName = "clock_" + selectedPos;
            updateAppWidget(context, appWidgetManager, appWidgetIds[i], resName);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        if (l.b.DEBUG) Log.d(TAG, "onDeleted");

        final int N = appWidgetIds.length;
        for (int i=0; i<N; i++) {
            AnalogAppWidgetConfigure.deleteClockPref(context, appWidgetIds[i]);
        }
    }

    @Override
    public void onEnabled(Context context) {
        if (l.b.DEBUG) Log.d(TAG, "onEnabled");
    }

    @Override
    public void onDisabled(Context context) {
        if (l.b.DEBUG) Log.d(TAG, "onDisabled");
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
            int appWidgetId, String resName) {
        if (l.b.DEBUG) Log.d(TAG, "updateAppWidget appWidgetId=" + appWidgetId);

        String packageName = context.getPackageName();
        Resources res = context.getResources();
        int layoutID = res.getIdentifier(resName, "layout", packageName);
        RemoteViews views = new RemoteViews(context.getPackageName(), layoutID);

        views.setOnClickPendingIntent(R.id.analogClock,
                PendingIntent.getActivity(context, 0,
                   DefaultPackages.getDefaultClockIntent(context), 0));

        appWidgetManager.updateAppWidget(appWidgetId, views);
    }


}

